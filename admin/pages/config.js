require.config({

    baseUrl: '/admin/pages/',

    waitSeconds: 30,

    urlArgs: "bust=v2",

    paths: {
        libsdir: '/bower_components',
        vendordir: '/vendor',

        json: '/bower_components/requirejs-plugins/src/json',
        text: '/bower_components/text/text',
        css: '/bower_components/require-css/css',

        jquery: '/bower_components/jquery/dist/jquery',
        underscore: '/bower_components/underscore/underscore',
        backbone: '/bower_components/backbone/backbone',
        'backbone-deep-model': '/bower_components/backbone-deep-model/dist/backbone-deep-model',
        fecha: '/bower_components/fecha/fecha',
        jqueryValidate: '/bower_components/jquery-validation/dist/jquery.validate',
        backgridPath: '/bower_components/backgrid/lib',
        backgrid: '/bower_components/backgrid/lib/backgrid',

        datetimepicker: '/bower_components/datetimepicker/jquery.datetimepicker',
        'jquery-mousewheel': '/bower_components/jquery-mousewheel/jquery.mousewheel',
        'php-date-formatter': '/bower_components/php-date-formatter/js/php-date-formatter',

        sightglass: '/bower_components/sightglass/index',
        rivets: '/bower_components/rivets/dist/rivets',

        vow: '/bower_components/vow/lib/vow',

        tinymce: '/bower_components/tinymce',

        jcrop: '/bower_components/Jcrop',

        components: '/components',
        common: '/admin/common',
        util: '/components/util',

        utils: '/admin/common/utils',
        'string-utils': '/admin/common/utils/string',
        'view-utils': '/admin/common/utils/view',

        formComponents: '/components/ui/form',

        ajax: '/admin/common/services/ajax/jquery.ajax',
        promise: '/admin/common/services/promise/vanilla.promise',
        router: '/admin/common/services/router/backbone.router',
        history: '/admin/common/services/history/backbone.history',
        draggable: '/admin/common/services/drag-n-drop/draggable',
        'image-resizer': '/admin/common/services/image/image-resizer',

        view: '/admin/common/views/backbone-view',
        'js-nav': '/admin/common/views/nav/js-nav',
        bindable: '/admin/common/views/mixins/bindable',
        hideable: '/admin/common/views/mixins/hideable',
        maskable: '/admin/common/views/mixins/maskable',
        sortable: '/admin/common/views/mixins/sortable',
        filtrable: '/admin/common/views/mixins/filtrable',
        navigationable: '/admin/common/views/mixins/navigationable',
        'page-views': '/admin/common/views/mixins/page-views',

        'ajax-model-api': '/admin/common/models/ajax-model-api',
        model: '/admin/common/models/Model',
        'deep-model': '/admin/common/models/DeepModel',
        collection: '/admin/common/models/Collection',
        pagination: '/admin/common/models/pagination',
        'ajax-model': '/admin/common/models/ajax-model',
        'ajax-deep-model': '/admin/common/models/ajax-deep-model',
        'ajax-collection': '/admin/common/models/ajax-collection',

        validator: '/admin/common/services/form.validator',
        'datetime-picker': '/admin/common/ui/datetimepicker/datetimepicker',
        modal: '/admin/common/ui/modal-new/jquery.modal',
        message: '/admin/common/ui/message/jquery.message',
        'image-editor': '/admin/common/ui/image-editor/image-editor',
        'thumbnail-editor': '/admin/common/ui/thumbnail-editor/thumbnail-editor',
        'tags-input': '/admin/common/ui/tags-input/tags-input',

        'styles-teachers-list': '/admin/styles/teachers-list',

    },

    packages: [

        {
            name: 'filters-panel',
            location: '/admin/common/ui/filters-panel',
            main: 'filters-view'
        },
        {
            name: 'items-grid',
            location: '/admin/common/views/items-grid',
            main: 'items-grid-view'
        },
        {
            name: 'items-list',
            location: '/admin/common/views/items-list',
            main: 'items-list-view'
        },
        {
            name: 'layout-view',
            location: '/admin/common/views/layout',
            main: 'layout-view'
        },
        {
            name: 'courses-selecting-list',
            location: '/admin/common/ui/courses-list',
            main: 'courses-list'
        },
        {
            name: 'pager',
            location: '/admin/common/ui/pager',
            main: 'pager'
        },

    ],

    shim: {
        underscore: {
            exports: '_'
        },

        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },

        sightglass: {
            exports: 'sightglass'
        },
        rivets: {
            deps: ['sightglass'],
            exports: 'rivets'
        },

        datetimepicker: {
            deps: ['css!datetimepicker', 'jquery-mousewheel', 'php-date-formatter']
        },

        validator: {
            deps: ['jqueryValidate']
        },

        modal: {
            deps: ['jquery', 'css!common/ui/modal-new/jquery.modal.css']
        },

        message: {
            deps: ['jquery', 'css!common/ui/message/jquery.message.css']
        },

        backgrid: {
            deps: ['backbone', 'underscore', 'css!backgridPath/backgrid.css']
        },

        'image-editor': {
            deps: [
                'jquery',
                'jcrop/js/Jcrop.min',
                'css!jcrop/css/Jcrop.css',
                'css!common/ui/image-editor/image-editor.css',
            ]
        },

    },

    config: {
        moment: {
            noGlobal: true
        }
    },

});

requirejs.onError = function(err) {
    if(err.requireType === 'timeout') {
        alert(err.message);
    }
    else {
        // alert(err);
        throw err;
    }
};
