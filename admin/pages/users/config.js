require([window.adminConfigUrl], function() {

    require.config({
        baseUrl: '/admin/pages/users/',

        packages: [
            {
                name: 'layout',
                main: 'layout-view'
            },
            {
                name: 'users-list',
                location: 'users-list',
                main: 'users-list-view'
            },
            {
                name: 'user-details',
                main: 'user-details-view'
            },
            {
                name: 'page',
                location: './',
                main: 'page'
            }
        ]
    });

    require([
        'backbone',
        'underscore',
        'rivets',
        './app-router',
    ], function() {
        require([
            'common/rivets/binders',
            'common/rivets/adapters',
            'common/rivets/formatters',
            'common/rivets/sorters',
            'common/rivets/date-formatters',

            'page',
        ]);
    });

});
