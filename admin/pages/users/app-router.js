define(function(require) {

    const app = Box.Application;

    const BaseRouter = require('router');
    const History = require('history');

    app.addService('history', function() {

        return History;

    });

    app.addService('router', function(app) {

        const Router = BaseRouter.extend({

            routes: {
                '(/)(?*queryString)': 'index',
                ':itemId(/)(?*queryString)': 'itemEditor',
            },

            index(queryString) {
                var queryParams = this.parseQueryString(queryString);
                app.broadcast('navigate', {
                    name: 'index',
                    queryParams,
                });
            },

            itemEditor(itemId, queryString) {
                var queryParams = this.parseQueryString(queryString);
                app.broadcast('navigate', {
                    name: 'item-editor',
                    itemId,
                    queryParams,
                });
            },

        });

        return new Router();

    });

});
