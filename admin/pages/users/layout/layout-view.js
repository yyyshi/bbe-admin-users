define(function(require) {

    const LayoutView = require('layout-view');

    const tpl = require('text!./layout.html');

    require('css!./layout.css');

    return LayoutView.extend({

        template: _.template(tpl),

    });

});
