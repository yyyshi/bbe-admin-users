define(function(require) {
    'use strict';

    const View = require('view');
    const hideable = require('hideable');
    const bindable = require('bindable');
    const maskable = require('maskable');
    const viewUtils = require('view-utils');
    const FormValidator = require('validator');
    const Message = require('message');
    const TagsInput = require('tags-input');

    const tpl = require('text!./user-details.html');
    require('css!./user-details.css');

    const tagsInputOptions = {
    };

    const ExtView = View
        .extend(bindable)
        .extend(hideable)
        .extend(maskable);

    return ExtView.extend({

        template: _.template(tpl),

        events: {
            'submit [data-role="add-to-course"]': 'onAddToCourseSubmit',
            'change #add-to-course': 'onShowAddToCourse',
        },

        initialize(config) {
            this.$el.html(this.template);

            _.extend(this, _.pick(config, 'item', 'courses', 'userToCourse'));

            this.$addToCourseForm = this.$('form[data-role="add-to-course"]');
            this.getAddToCourseFormData = getFormData.bind(null, this.$addToCourseForm);

            this.formValidator = FormValidator(this.$addToCourseForm[0]);

            const toggleAddToCourseButton = viewUtils.toggleClass.bind(null, 'button_disabled button_waiting', this.$addToCourseForm.find('[type="submit"]'));
            this.disableAddToCourseButton = toggleAddToCourseButton.bind(null, false);
            this.enableAddToCourseButton = toggleAddToCourseButton.bind(null, true);

            this.bind({
                item: this.item,
                courses: this.courses,
                view: this,
            });
        },

        getById(id) {
            this.mask();
            return this.item.getById(id)
                .then(() => {
                    this.disableAddedCourses();
                    this.unmask();
                })
                .catch(err => {
                    this.unmask();
                    throw err;
                });
        },

        fetchCourses() {
            return this.courses.query()
                .then(() => {
                    this.applyInput();
                    this.disableAddedCourses();
                })
                .catch(err => {
                    throw err;
                });
        },

        applyInput() {
            this.coursesInput = TagsInput(this.$('select[name="course_id"]'));
        },

        updateInput() {
            if( !this.coursesInput ) {
                return;
            }
            this.coursesInput.update();
        },

        addToCourse() {
            this.disableAddToCourseButton();
            this.userToCourse.postFormData(
                    this.getAddToCourseFormData()
                ).then(student => {
                    const user = this.item.get('user');
                    user.students.push(student);
                    this.item.set('user.students', user.students);
                    this.disableAddedCourses();
                    this.$addToCourseForm[0].reset();
                    this.enableAddToCourseButton();
                    this.trigger('added-to-course', this.item.toJSON(), _.clone(student));
                })
                .catch(err => {
                    this.formValidator.showError(err);
                    this.enableAddToCourseButton();
                    throw err;
                });
        },

        disableAddedCourses() {
            if( !this.item.get('user') ) {
                return;
            }
            const selectedCourses = this.item.get('user').students.map(item => item.enrollment.course);
            viewUtils.collectionsCompareAndSelect(this.courses, selectedCourses, viewUtils.idFiltrator);
            this.updateInput(); // обновим компонент
        },

        onAddToCourseSubmit(e) {
            e.preventDefault();
            this.addToCourse();
        },

        onShowAddToCourse(e) {
            this.fetchCourses();
        },

    });

    function getFormData(form) {
        return $(form).serializeArray();
    }

});
