define(function(require, exports, module) {
    'use strict';

    const app = Box.Application;

    const LayoutView = require('layout');
    const ListView = require('users-list');
    const EditView = require('user-details');
    const FiltersPanelView = require('filters-panel');

    const DataModel = require('ajax-deep-model');
    const DataStore = require('ajax-collection');

    function getApi(name) {
        return app.getGlobalConfig('api')[name];
    }

    return {

        makeLayout(el) {
            return new LayoutView({
                el
            });
        },

        makeUsersList(el) {
            return new ListView({
                el,
                items: new DataStore([], {
                    url: getApi('users')
                }),
                // Нужен только для создания нового
                item: new DataModel({}, {
                    url: getApi('user')
                }),
            });
        },

        makeFilters(el) {
            return new FiltersPanelView({
                el,
                availableFilters: [
                    {
                        type: 'tags',
                        name: 'course-subscribe',
                        label: 'Подписан на',
                        placeholder: 'Выберите курсы...',
                        multiple: true,
                        dataUrl: getApi('courses'),
                        dataProcessor(item) {
                            return {
                                key: item.id,
                                value: item.name,
                            }
                        },
                    },
                    {
                        type: 'tags',
                        name: 'enrollment-student',
                        label: 'Студент потока',
                        placeholder: 'Выберите потоки...',
                        multiple: true,
                        dataUrl: getApi('enrollments'),
                        dataProcessor(item) {
                            return {
                                group: item.course.id,
                                label: item.course.name,
                                key: item.id,
                                value: `${item.num}-й${item.beginAt ? ', ' + rivets.formatters.dateInterval(item.beginAt, item.endAt) : ''}`,
                            }
                        },
                    },
                    {
                        type: 'tags',
                        name: 'course-student',
                        label: 'Студент курса',
                        placeholder: 'Выберите курсы...',
                        multiple: true,
                        dataUrl: getApi('courses'),
                        dataProcessor(item) {
                            return {
                                key: item.id,
                                value: item.name,
                            }
                        },
                    },
                    {
                        type: 'tags',
                        name: 'not-course-student',
                        label: 'НЕ студент курса',
                        placeholder: 'Выберите курсы...',
                        multiple: true,
                        dataUrl: getApi('courses'),
                        dataProcessor(item) {
                            return {
                                key: item.id,
                                value: item.name,
                            }
                        },
                    },
                    {type: 'bool', name: 'has-enrollments', label: 'Студент'},
                    {type: 'text', name: 'search', label: 'Поиск', placeholder: 'Id, имя, email'},
                    {type: 'period', name: 'period', label: 'За период'},
                    {type: 'bool', name: 'subscribed', label: 'Подписан шк.'},
                    {type: 'date-between', name: 'subscribed-at', label: 'В школе с'},
                ],
            });
        },

        makeUserDetails(el) {
            return new EditView({
                el,
                item: new DataModel({}, {
                    url: getApi('user')
                }),
                courses: new DataStore([], {
                    url: getApi('courses')
                }),
                userToCourse: new DataModel({}, {
                    url: getApi('addToCourse')
                }),
            });
        },

    };

});
