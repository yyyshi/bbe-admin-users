﻿define(function(require) {
    'use strict';

    const app = Box.Application;

    const globals = require('globals');

    const pageViews = require('page-views');
    const viewsFactory = require('views-factory');
    const Message = require('message');
    const filtrable = require('filtrable');
    const navigationable = require('navigationable');

    function getApi(name) {
        return app.getGlobalConfig('api')[name];
    }

    app.addModule('page', function(ctx) {
        'use strict';

        let el;
        let views = {};

        const Page = Object.assign({},
            pageViews,
            filtrable,
            navigationable
        );

        return Object.assign(Page, {

            messages: [
                'navigate',
            ],

            init() {
                el = (ctx.getElement());

                this.pageViewsInit({
                    Message,
                });

                this.addView('layout', viewsFactory.makeLayout(el));
                const layout = this.getView('layout');
                layout.on('toolbar.button.click', this.onToolBarClick, this);

                this.addView('list', viewsFactory.makeUsersList(layout.getListBar()));
                const listView = this.getView('list');
                listView.on('item:select', this.onListItemSelect, this);
                listView.on('items:checked', _.throttle(this.onCheckItems, 100, {leading: false}), this);
                listView.on('pagination:page', this.onPaginationPage, this);
                listView.on('pagination:page-size', this.onPaginationPageSize, this);
                listView.on('error', this.error, this);


                this.addView('filter', viewsFactory.makeFilters(layout.getFilterBar()));
                const filterView = this.getView('filter');
                filterView.appendFilter('search');
                filterView.appendFilter('period');
                filterView.appendFilter('subscribed');
                filterView.appendFilter('subscribed-at');
                filterView.appendFilter('course-student');
                filterView.appendFilter('enrollment-student');
                filterView.appendFilter('course-subscribe');
                filterView.appendFilter('not-course-student');
                filterView.on('apply', this.onFilterApply, this);

                this.addView('edit', viewsFactory.makeUserDetails(layout.getEditBar()));
                const editView = this.getView('edit');
                editView.on('added-to-course', this.onUserAddedToCourse, this);
                editView.on('error', this.error, this);

                this.historyStart();
            },

            onCheckItems(checked, total, selectAll) {
                const btn = this.getView('layout').getToolbarButton('export-csv');
                btn.prop('disabled', !checked);

                const filterView = this.getView('filter');

                let checkedItems = this.getView('list').getChecked().items;

                if(checkedItems) {
                    checkedItems = checkedItems.map(function(item) {
                        return {
                            name: 'id',
                            mode: 'eq',
                            type: 'string',
                            value: item.get('id'),
                        };
                    });
                }

                const params = $.param({
                    filters: filterView.getValue(),
                    items: checkedItems,
                });
                btn.attr('data-href', `${getApi('usersCsv')}?${params}`);
            },

            onUserAddedToCourse(item, student) {
                let selectedItem;
                if((selectedItem = this.getView('list').getSelectedItem()) && selectedItem.get('id') == item.id) {
                    selectedItem.set(item);
                }
            },

            onToolBarClick(role, $el) {
                let view, data, msg;

                if($el.is('[data-href]')) {
                    window.open($el.attr('data-href'));
                }
            },

            destroy() {
                this.cleanViews();
                history = null;
                router = null;
                el = null;
                views = null;
            },

            onmessage(name, data) {
                var queryParams = data.queryParams;

                this.getView('list')
                    .query(queryParams)
                    .catch(this.error);

                if(data.name === 'item-editor') {
                    this.getView('list')
                        .selectItem(data.itemId);

                    this.getView('edit')
                        .show()
                        .getById(data.itemId)
                        .catch(this.error);
                }
                else {
                    this.getView('edit').hide();
                }

                if(queryParams.filters) {
                    this.applyFilters(queryParams.filters, this.getView('filter'));
                }
            },

        });

    });

    app.on('error', (e) => {
        const exception = e.exception;
        console.log('exception:', exception);
        console.error(e);
    });

    app.init(globals);

});
