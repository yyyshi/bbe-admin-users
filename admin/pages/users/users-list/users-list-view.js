define(function(require) {
    'use strict';

    const ItemsGridView = require('items-grid');

    const tpl = require('text!./users-list.html');
    require('css!./users-list.css');

    return ItemsGridView.extend({

        template: _.template(tpl),

        events: Object.assign({}, ItemsGridView.prototype.events, {
        }),

        initialize(config) {
            ItemsGridView.prototype.initialize.apply(this, arguments);
        },

        query(params) {
            return ItemsGridView.prototype.query.call(this, params);
        },

    });

});
