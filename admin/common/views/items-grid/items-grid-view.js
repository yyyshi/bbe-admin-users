define(function(require) {
    'use strict';

    const _ = require('underscore');
    const ItemsListView = require('items-list');
    const PagerView = require('pager');
    const Model = require('model');

    const viewUtils = require('view-utils');

    require('css!./items-grid.css');

    return ItemsListView.extend({

        events: Object.assign({}, ItemsListView.prototype.events, {
            'change [data-role="select-all"]': 'onSelectAllChange',
        }),

        initialize(config) {
            ItemsListView.prototype.initialize.apply(this, arguments);

            // Компенсирует ширину заголовка, по сравнению со списком. Колонки неровные из-за скроллбара
            this.$('.grid-header').css('margin-right', viewUtils.getScrollBarWidth());

            this.summary = new Model();
            this.checked = {
                all: false,
                items: [],
            };

            if(this.$pager = this.$('.grid__pager')) {
                this.pager = new PagerView({
                    el: this.$pager,
                    collection: this.items,
                });
                this.listenTo(this.pager, 'navigate:page', this.onPagerNavigate);
                this.listenTo(this.pager, 'navigate:page-size', this.onPageSizeNavigate);
            }

            this.listenTo(this.items, 'change:checked', _.throttle(this.onCheckedChange, 100, {leading: false}));

            this.updateBind({
                items: this.items,
                pagination: this.items.getPagination().data(),
                summary: this.summary,
                checked: this.checked,
                view: this,
            });
        },

        query(params) {
            return ItemsListView.prototype.query.call(this, params)
                .then(() => this.calculateSummary());
        },

        onPagerNavigate(page) {
            this.trigger('pagination:page', page);
        },

        onPageSizeNavigate(pageSize) {
            this.trigger('pagination:page-size', pageSize);
        },

        onRowClick(e) {
            // Если чекнули, то это не выбор сроки
            if(['input', 'label'].indexOf(e.target.tagName.toLowerCase()) !== -1) {
                return;
            }
            ItemsListView.prototype.onRowClick.apply(this, arguments);
        },

        onCheckedChange(item, checked) {
            if(checked) {
                this.checked.items.push(item);
            }
            else {
                if(this.checked.all) {
                    this.checked.items = this.items.slice();
                }
                this.checked.all = false;
                this.checked.items = this.checked.items.filter(it => !(it === item));
            }
            if(!this.checked.all) {
                this.trigger(
                    'items:checked',
                    this.checked.items.length,
                    this.items.getTotal(),
                    this.checked.all
                );
            }
        },

        onSelectAllChange(e) {
            this.checkAll(this.checked.all);
            if(!this.checked.all) {
                this.checked.items = [];
            }
            this.trigger(
                'items:checked',
                this.checked.all ? this.items.getTotal() : 0,
                this.items.getTotal(),
                this.checked.all
            );
        },

        /**
         * @param  {boolean} checked
         */
        checkAll(checked) {
            this.items.map(item => item.set('checked', checked));
        },

        getChecked() {
            return {
                count: this.checked.all ? this.items.getTotal() : this.checked.items.length,
                items: this.checked.all ? null : this.checked.items.slice(),
            };
        },

        /**
         * Пересчет summary
         */
        calculateSummary() {
            // empty
        },

    });

});
