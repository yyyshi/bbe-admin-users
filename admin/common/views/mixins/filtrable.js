define(function(require) {
    'use strict';

    return {

        // Применяем фильтры
        applyFilters(filters, filterView) {
            let currentFilters = filterView.getFilters();

            filters.forEach(function(f) {
                let filter;
                if(filter = _.find(currentFilters, it => it.getName() == f.name)) {
                    filter.setInputValue(f.value);
                    filter.setEqMode(f.mode);
                    currentFilters = _.without(currentFilters, filter); // Удалим из списка уже примененный фильтр
                }
                else {
                    filter = filterView.appendFilter(f.name, {
                        value: f.value,
                        mode: f.mode
                    });
                }
            });
        }

    };

});
