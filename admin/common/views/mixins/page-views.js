define(function(require) {
    'use strict';

    let Message = function(message, opts = {}) {
        alert(`**Please, overwrite opts.Message**\n\n${message}`);
    };

    return {

        pageViewsInit(opts = {}) {
            Message = opts.Message || Message;
            this.views = this.views || {};
        },

        addView(name, view) {
            this.views[name] = view;
            return this;
        },

        getView(name) {
            return this.views[name];
        },

        cleanViews() {
            this.views.forEach(view => view.off().remove());
            views = null;
        },

        error(err) {
            let message = err.message || err.code || err;
            Message(message, {
                buttons: 'close',
                type: 'error',
            });
            console.error(err);
        },

    };

});
