define(function(require) {
    'use strict';

    return {

        /**
         * View's visible status
         * @return {Boolean} [description]
         */
        isVisible() {
            return this.$el.is(':visible');
        },


        /**
         * Show view
         * @event view:show
         * @return {[type]} [description]
         */
        show() {
            this.$el.show();
            this.trigger('view:show', this);
            return this;
        },

        /**
         * Hide view
         * @event view:hide
         * @return {[type]} [description]
         */
        hide() {
            this.$el.hide();
            this.trigger('view:hide', this);
            return this;
        },

    };

});
