define(function(require) {
    'use strict';

    require('css!./maskable.css');

    return {

        maskClass() {
            return 'maskable-view';
        },

        isMasked() {
            return this.$el.hasClass(
                this.maskClass()
            );
        },

        mask() {
            this.$el.addClass(
                this.maskClass()
            );
            this.createMask();
            this.trigger('view:masked', this);
            return this;
        },

        unmask() {
            this.$el.removeClass(
                this.maskClass()
            );
            this.removeMask();
            this.trigger('view:unmasked', this);
            return this;
        },

        createMask() {
            this.$mask = $('<div class="maskable-view__mask" />').appendTo(this.$el);
            return this;
        },

        removeMask() {
            this.$mask.remove();
            return this;
        },

    };

});
