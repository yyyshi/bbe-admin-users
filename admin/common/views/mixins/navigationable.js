define(function(require) {
    'use strict';

    const app = Box.Application;
    const router = app.getService('router');
    const history = app.getService('history');

    return {

        historyStart() {
            history.start({
                pushState: true,
                root: app.getGlobalConfig('routes').root,
            });
            this.history = history;
        },

        navigate(url = null, queryParams = {}) {
            var fragmentParts = router.splitUrl(this.history.fragment);
            url = url || fragmentParts.url;
            queryParams = Object.assign({}, fragmentParts.params, queryParams);

            router.navigate(
                router.buildUrl(url, queryParams),
                true
            );
        },

        onPaginationPage(page) {
            this.navigate(null, {
                page
            });
        },

        onPaginationPageSize(limit) {
            this.navigate(null, {
                limit
            });
        },

        onFilterApply(filters) {
            this.navigate(null, {
                filters,
                page: 1, // Сбросим текущую страницу в 1
            });
        },

        onListItemSelect(id) {
            this.navigate(id);
        },

    };

});
