define(function(require) {
    'use strict';

    require('css!./sortable.css');

    return {

        // Инициализация (необходимо вызвать после рендеринга view)
        sortableInit() {
            this.events = $.extend({}, this.events, {
                'click [data-sort-name]': 'onSortColumnClick'
            });
            this.delegateEvents();
        },

        // Обработчик клика на колонку
        onSortColumnClick(e) {
            const $column = $(e.currentTarget);
            let order;

            // Активна - реверс
            if(this.isColumnSortActive($column)) {
                order = this.reverseColumnOrder(this.getColumnOrder($column));
            }
            // Неактивна - текущее значение
            else {
                order = this.getColumnOrder($column);
            }

            this.setColumnOrder($column, order);
            this.switchSortColumns($column);

            this.trigger('sortable:order', this.getSort());
        },

        // Инвертирует порядок сортировки
        reverseColumnOrder(order) {
            return order.toLowerCase() === 'asc' ? 'desc' : 'asc';
        },

        // Находит колонку по имени
        findSortColumn(name) {
            return this.$(`[data-sort-name="${name}"]`);
        },

        // Активирует колонку $column (отключает старую активную)
        switchSortColumns($column) {
            if(!this.getActiveSortColumn().is($column)) {
                this.deactivateSortColumn(this.getActiveSortColumn());
                this.activateSortColumn($column);
            }
        },

        // Возвращает значение порядка сортировки для $column
        getColumnOrder($column) {
            return $column.attr('data-sort-order') || 'asc';
        },

        // Устанавливает порядок сортировки для $column
        setColumnOrder($column, order = 'asc') {
            $column.attr('data-sort-order', order.toLowerCase());
        },

        // Активна ли $column
        isColumnSortActive($column) {
            return $column.is('[data-sort-active]');
        },

        // Возвращает текущую активную колонку
        getActiveSortColumn() {
            return this.$('[data-sort-active]');
        },

        // Активирует колонку $column
        activateSortColumn($column) {
            $column.attr('data-sort-active', true);
        },

        // Деактивирует колонку $column
        deactivateSortColumn($column) {
            $column.removeAttr('data-sort-active');
        },

        // Возвращает имя колонки $column
        getSortColumnName($column) {
            return $column.attr('data-sort-name');
        },

        // Возвращает текущие имя и порядок сортировки
        getSort() {
            const $activeColumn = this.getActiveSortColumn();
            return {
                name: this.getSortColumnName($activeColumn),
                order: this.getColumnOrder($activeColumn),
            };
        },

        // Устанавливает текущие имя и порядок сортировки
        setSort(name, order) {
            const $column = this.findSortColumn(name);
            this.setColumnOrder($column, order);
            this.switchSortColumns($column);
        },

    };

});
