define(function(require) {
    'use strict';

    require('rivets');

    return {

        bind(model) {
            this.binding = rivets.bind(this.el, model);
        },

        unbind() {
            if(this.binding) {
                this.binding.unbind(this.el);
            }
        },

        updateBind(model) {
            if(this.binding) {
                this.binding.update(model);
            }
        },

    };

});
