define(function(require) {

    const View = require('view');

    const stringUtils = require('string-utils');

    require('css!./layout.css');

    function getBar($el) {
        return $el;
    }

    return View.extend({

        events: {
            'click .layout__toolbar .toolbar .button': 'onToolbarButtonClick',
        },

        initialize() {
            this.$el
                .addClass('layout')
                .html(this.template);

            this.$('[data-role]').each((idx, item) => {
                let matches;
                if(matches = item.dataset.role.match(/^([a-z\-]+)\-bar$/i)) {
                    this[`get${stringUtils.studly(matches[1])}Bar`] = getBar.bind(this, $(item));
                }
            });
        },

        getToolBar() {
            if(!this.$toolBar) {
                this.$toolBar = this.$('.toolbar');
            }
            return this.$toolBar;
        },

        getToolbarButton(role) {
            return this.getToolBar().find(`[data-role="${role}"]`);
        },

        onToolbarButtonClick(e) {
            const t = e.target;
            this.trigger('toolbar.button.click', t.dataset.role, $(t));
        },

    });

});
