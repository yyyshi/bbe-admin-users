define(function(require) {
    'use strict';

    const View = require('view');
    const bindable = require('bindable');
    const maskable = require('maskable');

    require('css!./items-list.css');

    const ExtView = View
        .extend(maskable)
        .extend(bindable);

    return ExtView.extend({

        events: {
            'click [data-role="list-row"]': 'onRowClick',
        },

        // config.items {ajax-collection} - список элементов
        // config.item {ajax-model} (optional) - создание нового элемента
        initialize(config) {
            this.$el.html(this.template);

            _.extend(this, _.pick(config, 'items', 'item'));

            this.bind({
                items: this.items,
                view: this,
            });
        },

        query(params) {
            this.clearSelection();
            this.mask();
            return this.items.query(params)
                .then(data => {
                    this.unmask();
                    this.scrollToSelectedIfHidden();
                    return data;
                })
                .catch(err => {
                    throw err;
                });
        },

        onRowClick(e) {
            const id = e.currentTarget.dataset.id;
            this.selectItem(id);
            this.triggerItemSelect(id);
        },

        triggerItemSelect(id) {
            this.trigger('item:select', id);
        },

        selectItem(id) {
            this.selectedItem = id;
        },

        // Подскролливаем только если элемент невидим
        scrollToSelectedIfHidden() {
            const $el = this.$(`[data-id="${this.selectedItem}"]`);
            if(!$el.length) {
                return;
            }
            const pos = $el.position();
            if(!pos) {
                return;
            }
            const top = pos.top;
            if(top >= 0 && top < this.$el.height()) {
                return;
            }
            this.$el.stop().animate({ scrollTop: this.$el.scrollTop() + top - this.$el.position().top }, 100);
        },

        clearSelection() {
            this.selectedItem = null;
        },

        getSelectedItem() {
            return this.items.get(this.selectedItem);
        },

        createItem(data) {
            return this.item.postFormData(data);
        },

        set(items, opts = { add: false, remove: false, merge: true }) {
            this.items.set(items, opts);
            this.sort();
        },

        sort() {
            this.items.sort();
        },

    });

});
