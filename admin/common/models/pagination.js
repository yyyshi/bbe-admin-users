define(function(require, exports, module) {
    'use strict';

    return function() {

        let data = {
            total: 0,
            count: 0,
            perPage: 0,
            currentPage: 1,
            totalPages: 1,
            maxPage: 1,
            minPage: 1,
        };

        function setGet(name, val) {
            if(val === undefined) {
                return data[name];
            }
            data[name] = val;
        }

        const methods = {
            total: setGet.bind(null, 'total'),
            count: setGet.bind(null, 'count'),
            perPage: setGet.bind(null, 'perPage'),
            currentPage: setGet.bind(null, 'currentPage'),
            totalPages: setGet.bind(null, 'totalPages'),
            maxPage: setGet.bind(null, 'maxPage'),
            minPage: setGet.bind(null, 'minPage'),
        };

        return Object.assign({

            reset(data) {
                methods.total(data['total']);
                methods.count(data['count']);
                methods.perPage(data['perPage']);
                methods.currentPage(data['currentPage']);
                methods.totalPages(data['totalPages']);
                methods.maxPage(data['maxPage']);
                methods.minPage(data['minPage']);
            },

            data() {
                return data;
            },

        }, methods);
    };

});
