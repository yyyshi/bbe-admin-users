﻿define(function(require, exports, module) {
    'use strict';

    const Model = require('model');
    const ajaxModelApi = require('ajax-model-api');

    return Model.extend(ajaxModelApi);

});
