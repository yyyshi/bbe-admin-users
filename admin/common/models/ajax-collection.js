﻿define(function(require, exports, module) {
    'use strict';

    const Collection = require('collection');
    const Model = require('model');
    const Pagination = require('pagination');
    const {extend, isEqual} = require('underscore');
    const promise = require('promise');
    const {ajax} = require('ajax');

    return Collection.extend({

        /**
         *  api: {
         *      create
         *      read
         *      update
         *      delete
         *  }
         */
        initialize(models, options) {
            this.url = options.url;
            this.api = options.api || null;

            this._page = options.page || null;
            this._limit = options.limit || 10;
            this._orderBy = options.orderBy;
            this._direction = options.direction;
            this._filters = options.filters || {};
            // this._total = options.total;

            this._pagination = new Pagination();
        },

        /**
         * GET-запрос к серверу с параметрами data
         * @param  {Object}  data        Параметры запроса
         * @param  {Boolean} forceReload Принудительно перезагрузить данные
         * @return {promise}
         */
        query(data, forceReload = false) {
            this._mergeState(data || {});
            var params = this._mergeOptions();

            return new promise((resolve, reject) => {

                // Параметры запроса такие же как в прошлый раз и не надо перезапрашивать
                if(isEqual(params, this.prevParams) && !forceReload) {
                    resolve(this.prevResponse && this.prevResponse.data);
                    return;
                }

                ajax({
                    url: this.url,
                    type: 'GET',
                    data: params,
                })
                .then(response => {
                    this.prevParams = params;
                    this.prevResponse = response;
                    this._mergeState({ orderBy: response.orderBy });
                    if(response.pagination) {
                        this._pagination.reset(response.pagination);
                    }
                    else {
                        const len = response.data ? response.data.length : 0;
                        this._pagination.total(len);
                        this._pagination.count(len);
                        this._pagination.perPage(len);
                    }
                    this.reset(response.data);
                    // this._pagination = response.pagination;
                    resolve(response.data);
                })
                .catch(response => {
                    console.error(response);
                    const error = (response.error && response.error && response.error.message) || response;
                    console.error('Error:', error);
                    reject(error);
                });
            });
        },

        getById(id) {
            var that = this;
            return new promise(function(resolve, reject) {
                ajax({
                    url: that.url + id + '/',
                    type: 'GET',
                })
                .then(function(response) {
                    that.reset(response.data);
                    resolve(response.data);
                })
                .catch(function(response) {
                    console.log(response);
                    console.error('Error:', response.error.message);
                    reject(response.error);
                });
            });
        },

        page(page) {
            page = page || this._page;
            return this.query({ page: page });
        },

        // Search by string
        search(str) {
            return this.query(
                this._mergeOptions({ filters: { search: str } })
            ); // , page: null
        },

        postFormData(data) {
            var that = this;
            return new promise(function(resolve, reject) {
                ajax({
                    url: that.url,
                    type: 'POST',
                    data: data
                })
                .then(function(response) {
                    resolve(response.data);
                })
                .catch(function(response) {
                    var error = response.error ? response.error : response;
                    console.error('Error:', error.message ? error.message : error);
                    reject(error);
                });
            });
        },

        putFormData(id, data) {
            var that = this;
            return new promise(function(resolve, reject) {
                ajax({
                    url: that.url + id + '/',
                    type: 'PUT',
                    data: data
                })
                .then(function(response) {
                    resolve(response.data);
                })
                .catch(function(response) {
                    var error = response.error ? response.error : response;
                    console.error('Error:', error.message ? error.message : error);
                    reject(error);
                });
            });
        },

        delete(id) {
            var that = this;
            return new promise(function(resolve, reject) {
                ajax({
                    url: that.getUrl('delete') + id + '/',
                    type: 'DELETE'
                })
                .then(function(response) {
                    resolve(response.data);
                })
                .catch(function(response) {
                    var error = response.error ? response.error : response;
                    console.error('Error:', error.message ? error.message : error);
                    reject(error);
                });
            });
        },

        _mergeState(options) {
            if(options) {
                this._page = options.page || null;
                this._limit = options.limit || null;
                this._orderBy = options.orderBy || this._orderBy;
                this._direction = options.direction || this._direction;
                this._filters = options.filters || this._filters;
            }
            return options;
        },

        _mergeOptions(options) {
            // Deep extend
            return extend({}, {
                page: this._page,
                limit: this._limit,
                orderBy: this._orderBy,
                direction: this._direction,
                filters: this._filters,
            }, options);
        },

        getUrl(action = null) {
            if(!action) {
                return this.url;
            }
            if(this.api && this.api[action]) {
                return this.api[action];
            }

            return this.url;
        },

        getTotal() {
            return this.getPagination().total();
        },

        getCount() {
            return this.getPagination().count();
        },

        getPagination() {
            return this._pagination;
        },

        getOrderBy() {
            return this._orderBy;
        },

    });

});
