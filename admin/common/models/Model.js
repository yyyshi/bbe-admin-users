define(function(require) {
    'use strict';

    const Model = require('backbone').Model;
    const extend = require('underscore').extend;

    extend(Model.prototype, {

        // Deep get
        get(key) {
            try {
                return _.reduce(key.split('.'), function(attr, key) {
                    if(attr instanceof Model) {
                        return attr.attributes[key];
                    }

                    return attr[key];
                }, this.attributes);
            }
            catch (e) {
                return undefined;
            }
        },

    });

    return Model;

});
