﻿define(function(require, exports, module) {
    'use strict';

    const DeepModel = require('deep-model');
    const ajaxModelApi = require('ajax-model-api');
    const _ = require('underscore');

    /**
     * Очистка массивов deep model
     * @param  {Object} options
     * @return {Object}
     */
    ajaxModelApi.clear = function(options) {
        DeepModel.prototype.clear.apply(this, arguments);
        // No triggering events
        this.attributes = {};
        if(this.defaults) {
            this.attributes = _.clone(this.defaults);
        }
        return this;
    };

    return DeepModel.extend(ajaxModelApi);

});
