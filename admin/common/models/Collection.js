define(function(require) {

    const Collection = require('backbone').Collection;
    const Model = require('model');
    const extend = require('underscore').extend;

    extend(Collection.prototype, {

        model: Model,

    });

    return Collection;

});
