﻿define(function(require, exports, module) {
    'use strict';

    const promise = require('promise');
    const {ajax} = require('ajax');

    return {

        /**
         *  api: {
         *      create
         *      read
         *      update
         *      delete
         *  }
         */
        initialize(model, options = {}) {
            this.url = options.url;
            this.api = options.api || null;
            if(options.defaults) {
                this.defaults = options.defaults;
            }
        },

        queryAction(id, action, data = {}) {
            let url = `${this.url}${id}/${action}/`;

            return new promise((resolve, reject) => {

                ajax({
                    url,
                    type: 'GET',
                    data,
                })
                .then(response => {
                    resolve(response.data ? response.data : response);
                })
                .catch(response => {
                    console.error(response);
                    const error = (response.error && response.error && response.error.message) || response;
                    reject(error);
                });
            });
        },

        /**
         * GET-запрос к серверу
         * @param  {mixed}   id        ID
         * @param  {Boolean} forceReload Принудительно перезагрузить данные
         * @return {promise}
         */
        getById(id, forceReload = false) {
            return new promise((resolve, reject) => {

                // Параметры запроса такие же как в прошлый раз и не надо перезапрашивать
                if((id === this.prevId) && !forceReload) {
                    resolve(this.prevResponse && this.prevResponse.data);
                    return;
                }

                ajax({
                    url: this.url + id + '/',
                    type: 'GET',
                })
                .then(response => {
                    this.prevId = id;
                    this.prevResponse = response;
                    this.clear();
                    this.set(
                        this.parse(response.data)
                    );
                    resolve(response.data);
                })
                .catch(response => {
                    console.error(response);
                    const error = (response.error && response.error && response.error.message) || response;
                    reject(error);
                });
            });
        },

        postFormData(data, options = {}) {
            return new promise((resolve, reject) => {
                ajax($.extend({}, {
                    url: this.url,
                    type: 'POST',
                    data: data
                }, options))
                .then(response => {
                    resolve(response.data);
                })
                .catch(response => {
                    console.error(response);
                    const error = (response.error && response.error && response.error.message) || response;
                    reject(error);
                });
            });
        },

        putFormData(id, data, options = {}) {
            return new promise((resolve, reject) => {
                ajax($.extend({}, {
                    url: this.url + id + '/',
                    type: 'PUT',
                    data: data
                }, options))
                .then(response => {
                    resolve(response.data);
                })
                .catch(response => {
                    console.error(response);
                    const error = (response.error && response.error && response.error.message) || response;
                    reject(error);
                });
            });
        },

        delete(id) {
            return new promise((resolve, reject) => {
                ajax({
                    url: this.getUrl('delete') + id + '/',
                    type: 'DELETE'
                })
                .then(response => {
                    resolve(response.data);
                })
                .catch(response => {
                    console.error(response);
                    const error = (response.error && response.error && response.error.message) || response;
                    reject(error);
                });
            });
        },

        getUrl(action) {
            if(!action) {
                return this.url;
            }
            if(this.api && this.api[action]) {
                return this.api[action];
            }

            return this.url;
        },

    };

});
