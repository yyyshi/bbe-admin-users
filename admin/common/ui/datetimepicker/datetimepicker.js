define(function(require) {
    'use strict';

    var $ = require('jquery');
    require('datetimepicker');

    var defaults = {
        scrollInput: false,
        format: 'Y-m-d',
        timepicker: false,
        bindEl: null, // Привязанный к el элемент (для зависимых контролов)
    };

    function setProperty($el, property) {
        return function(ct) {
            var opts = {};
            opts[property] = ($el.val() ? $el.val() : false);
            this.setOptions(opts);
        };
    }

    /**
     * Usage:
     *      var $picker = DateTimePicker(this.$('[name="discountTill"]'), this.datetimepickerOptions);
     *      var $picker = DateTimePicker(this.$('[name="beginAt"]'), $.extend({}, this.datetimepickerOptions, {
     *          bindEl: this.$('[name="endAt"]')
     *      }));
     *      $picker.destroy();
     *
     */
    var DateTimePicker = function(el, options) {
        var opts = $.extend({}, defaults, options);
        var $el = $(el);
        var $bindEl;

        if(opts.bindEl) {
            $bindEl = $(opts.bindEl);
            opts.onShow = setProperty($bindEl, 'maxDate');
        }

        $el.datetimepicker(opts);

        if(opts.bindEl) {
            opts.onShow = setProperty($el, 'minDate');
            $bindEl.datetimepicker(opts);
        }

        return {

            destroy() {
                $el.datetimepicker('destroy');
                if($bindEl) $bindEl.datetimepicker('destroy');
            },

            reset() {
                $el.datetimepicker('reset');
                if($bindEl) $bindEl.datetimepicker('reset');
            },

            value() {
                return $el.val();
            },

        };
    };

    return DateTimePicker;
});
