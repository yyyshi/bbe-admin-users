define(function(require) {
    'use strict';

    var $ = require('jquery');

    require('libsdir/chosen/chosen.jquery');
    require('css!libsdir/chosen/chosen.css');
    require('css!./tags-input.css');

    // https://harvesthq.github.io/chosen/options.html
    var defaults = {
        search_contains: true,
        // single_backstroke_delete: false,
        // display_selected_options: false,
    };

    /**
     * Usage:
     *
     */
    var TagsInput = function(el, options) {
        var opts = $.extend({}, defaults, options);
        var $el = $(el);

        $el.chosen(opts);

        return {

            destroy() {
                $el.chosen('destroy');
            },

            update() {
                $el.trigger('chosen:updated');
            },

            value() {
                return $el.val();
            },

        };
    };

    return TagsInput;
});
