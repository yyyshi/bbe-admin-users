define(function(require) {
    'use strict';

    const _ = require('underscore');

    const Base = require('../base/base-filter');
    const DateTimePicker = require('datetime-picker');

    const tpl = require('text!./tpl/date-between-filter.html');
    require('css!./styles/date-between-filter.css');

    const datetimepickerOptions = {
        timepicker: false,
        format: 'Y-m-d',
        allowBlank: true,
        maxDate: 0,
    };

    return Base.extend({

        template: _.template(tpl),

        className: 'date-between-filter',

        inputAttrs: _.union([], Base.prototype.inputAttrs, [
        ]),

        render() {
            Base.prototype.render.apply(this);

            const $inputs = this.getInputEl();
            // const $from = $($inputs[0]);
            // const $till = $($inputs[1]);

            const datePickerFrom = DateTimePicker(
                this.$('.date-filter__date-input')[0],
                _.extend({}, datetimepickerOptions, {
                    onShow() {
                        this.setOptions({
                            maxDate: datePickerTill.value() ? datePickerTill.value() : false
                        });
                    }
                })
            );
            const datePickerTill = DateTimePicker(
                this.$('.date-filter__date-input')[1],
                _.extend({}, datetimepickerOptions, {
                    onShow() {
                        this.setOptions({
                            minDate: datePickerFrom.value() ? datePickerFrom.value() : false
                        });
                    }
                })
            );

            this.datePickerFrom = datePickerFrom;
            this.datePickerTill = datePickerTill;

            this.trigger('rendered');
        },

        /**
         * Возвращает главный input-элемент фильтра, относительно this.$el
         * @return {[type]} [description]
         */
        getInputEl() {
            return this.$('input');
        },

        getEqMode() {
            return 'between';
        },

        /**
         * Тип значения фильтра
         * @return string
         */
        getType() {
            return 'dates';
        },

        /**
         * Пустое ли значение фильтра
         * @return {Boolean}
         */
        isEmpty() {
            const $inputs = this.getInputEl();
            return !$($inputs[0]).val().trim().length || !$($inputs[1]).val().trim().length;
        },

        getValue() {
            let value = Base.prototype.getValue.call(this);
            const $inputs = this.getInputEl();
            value.value = [$($inputs[0]).val().trim(), $($inputs[1]).val().trim()];
            return value;
        },

        setInputValue(value) {
            const $inputs = this.getInputEl();
            $($inputs[0]).val(value[0]);
            $($inputs[1]).val(value[1]);
        },

    });

});
