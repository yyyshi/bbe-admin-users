define(function(require) {

    const _ = require('underscore');
    const View = require('backbone').View;

    return View.extend({

        tag: 'div',

        inputAttrs: [
        ],

        isRendered: false,

        initialize(options) {
            this.options = _.extend({}, options);
            this.listenTo(this, 'rendered', () => this.isRendered = true);
            this.render();
        },

        render() {
            this.$el.html(
                this.template(this.getRenderData())
            );
            this.setInputAttrs(
                _.pick(this.options, this.inputAttrs)
            );
            // this.trigger('rendered'); // Перегружаем в наследнике, вызываем оттуда этот метод и в наследнике же триггерим
            return this;
        },

        /**
         * Добавляет html-атрибуты к главному input-элементу фильтра
         * @param {Object} attrs [description]
         */
        setInputAttrs: function(attrs = {}) {
            this.getInputEl().attr(attrs);
        },

        /**
         * Возвращает главный input-элемент фильтра, относительно this.$el
         * @return {[type]} [description]
         */
        getInputEl() {
            return this.$el.children();
        },

        /**
         * Возвращает данные для рендеринга template
         * @return {Object}
         */
        getRenderData() {
            return {};
        },

        getName() {
            return this.options.name;
        },

        getValue() {
            return {
                name: this.getName(),
                mode: this.getEqMode(),
                type: this.getType(),
                value: this.getInputEl().val(),
            }
        },

        setInputValue(value) {
            this.getInputEl().val(value);
            // console.log(this.getInputEl(), this.getInputEl().val());
        },

        setEqMode(mode) {
            // Пустой, все верно
        },

        /**
         * Сравнение
         * @return {string} ['eq', 'lt', 'gt']
         */
        getEqMode() {
            return 'eq';
        },

        /**
         * Тип значения фильтра
         * @return string
         */
        getType() {
            return 'string';
        },

        /**
         * Пустое ли значение фильтра
         * @return {Boolean}
         */
        isEmpty() {
            return !this.getInputEl().val().trim().length;
        },

    });

});
