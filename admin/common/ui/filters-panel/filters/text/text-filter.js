define(function(require) {
    'use strict';

    const _ = require('underscore');

    const Base = require('../base/base-filter');

    const tpl = require('text!./tpl/text-filter.html');
    require('css!./styles/text-filter.css');

    return Base.extend({

        template: _.template(tpl),

        className: 'text-filter',

        inputAttrs: _.union([], Base.prototype.inputAttrs, [
            'placeholder',
            'multiple',
        ]),

        render() {
            Base.prototype.render.apply(this);
            this.trigger('rendered');
        },

        /**
         * Возвращает главный input-элемент фильтра, относительно this.$el
         * @return {[type]} [description]
         */
        getInputEl() {
            return this.$('input');
        },

    });

});
