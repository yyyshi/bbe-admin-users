define(function(require) {
    'use strict';

    const _ = require('underscore');

    const Base = require('../base/base-filter');

    const tpl = require('text!./tpl/period-filter.html');
    require('css!./styles/period-filter.css');

    const tagsInputOptions = {
    };

    return Base.extend({

        template: _.template(tpl),

        className: 'period-filter',

        inputAttrs: _.union([], Base.prototype.inputAttrs, [
            'placeholder',
        ]),

        render() {
            Base.prototype.render.apply(this);
            this.trigger('rendered');

            return this;
        },

        /**
         * Тип значения фильтра
         * @return string
         */
        getType() {
            return 'string';
        },

        /**
         * Возвращает главный input-элемент фильтра, относительно this.$el
         * @return {[type]} [description]
         */
        getInputEl() {
            return this.$('select');
        },

        /**
         * Возвращает данные для рендеринга template
         * @return {Object}
         */
        getRenderData() {
            return {
                items: this.items,
                options: this.options,
            };
        },

    });

});
