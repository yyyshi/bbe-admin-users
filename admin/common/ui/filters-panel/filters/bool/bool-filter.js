define(function(require) {
    'use strict';

    const _ = require('underscore');

    const Base = require('../base/base-filter');

    const tpl = require('text!./tpl/bool-filter.html');
    require('css!./styles/bool-filter.css');

    return Base.extend({

        template: _.template(tpl),

        className: 'bool-filter',

        inputAttrs: _.union([], Base.prototype.inputAttrs, [
        ]),

        render() {
            Base.prototype.render.apply(this);
            this.trigger('rendered');
        },

        /**
         * Возвращает главный input-элемент фильтра, относительно this.$el
         * @return {[type]} [description]
         */
        getInputEl() {
            return this.$('select');
        },

        /**
         * Тип значения фильтра
         * @return string
         */
        getType() {
            return 'boolean';
        },

    });

});
