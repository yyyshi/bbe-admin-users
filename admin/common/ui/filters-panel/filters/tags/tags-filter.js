define(function(require) {
    'use strict';

    const _ = require('underscore');
    const promise = require('promise');
    const Collection = require('ajax-collection');

    const Base = require('../base/base-filter');
    const TagsInput = require('tags-input');

    const tpl = require('text!./tpl/tags-filter.html');
    require('css!./styles/tags-filter.css');

    const tagsInputOptions = {
    };

    return Base.extend({

        template: _.template(tpl),

        className: 'tags-filter',

        inputAttrs: _.union([], Base.prototype.inputAttrs, [
            'placeholder',
        ]),

        render() {
            _.extend(this, _.pick(this.options, 'dataProcessor'));

            if(this.options.dataUrl) {
                const collection = new Collection({}, { url: this.options.dataUrl });
                collection.query()
                    .then(data => {
                        this.items = this.processData(this.dataProcessor, data);
                        this.applyInput.apply(this);
                        this.trigger('rendered');
                    })
                    .catch((err) => {
                        throw err;
                    });
            }
            else {
                this.applyInput.apply(this);
                this.trigger('rendered');
            }

            return this;
        },

        applyInput() {
            Base.prototype.render.apply(this);
            this.tagsInput = TagsInput(this.getInputEl());
        },

        /**
         * Возвращает данные для рендеринга template
         * @return {Object}
         */
        getRenderData() {
            return {
                items: this.items,
                options: this.options,
            };
        },

        processData(processor, data) {

            // Группировка
            function addToGroup(list, id, label, item) {
                let idx = _.findIndex(list, el => el.group === id);
                if(idx === -1) {
                    list.push({
                        group: id,
                        label,
                        items: [],
                    });
                    idx = list.length - 1;
                }
                list[idx]['items'].push(_.pick(item, 'key', 'value'));

                return list;
            }

            function maybeGrouping(carry, item) {
                if(item.group) {
                    return addToGroup(carry, item.group, item.label, item);
                }

                carry.push(item);
                return carry;
            }

            // return data.reduce((carry, item) => _.compose(group.bind(null, carry), processor)(item), []);
            return data.map(processor) // Обработка
                .reduce(maybeGrouping, []); // Группировка (если нужно)
        },

        /**
         * Дефолтовый обработчик данных
         * (переназначается в options.dataProcessor)
         * @param  {mixed} item [description]
         * @return {mixed}      [description]
         */
        dataProcessor(item) {
            return item;
        },

        /**
         * Тип значения фильтра
         * @return string
         */
        getType() {
            return 'array';
        },

        /**
         * Возвращает главный input-элемент фильтра, относительно this.$el
         * @return {[type]} [description]
         */
        getInputEl() {
            return this.$('select');
        },

        setInputValue(value) {

            function applyValue(value) {
                Base.prototype.setInputValue.call(this, value);
                this.getInputEl().val(value);
                this.tagsInput.update();
            }

            if(this.isRendered) {
                applyValue.call(this, value);
            }
            else {
                this.once('rendered', applyValue.bind(this, value));
            }
        },

        /**
         * Пустое ли значение фильтра
         * @return {Boolean}
         */
        isEmpty() {
            return !this.getInputEl().val() || !this.getInputEl().val().length;
        },

    });

});
