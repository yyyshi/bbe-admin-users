define(function(require) {
    'use strict';

    const _ = require('underscore');

    const Base = require('../base/base-filter');
    const DateTimePicker = require('datetime-picker');

    const tpl = require('text!./tpl/date-filter.html');
    require('css!./styles/date-filter.css');

    const datetimepickerOptions = {
        timepicker: false,
        format: 'Y-m-d',
        allowBlank: true,
        maxDate: 0,
    };

    return Base.extend({

        template: _.template(tpl),

        className: 'date-filter',

        inputAttrs: _.union([], Base.prototype.inputAttrs, [
        ]),

        render() {
            Base.prototype.render.apply(this);

            this.$comparsion = this.$('[data-role="comparsion"]');

            this.datePicker = DateTimePicker(this.$('.date-filter__date-input'), datetimepickerOptions);

            this.trigger('rendered');
        },

        /**
         * Возвращает главный input-элемент фильтра, относительно this.$el
         * @return {[type]} [description]
         */
        getInputEl() {
            return this.$('input');
        },

        getEqMode() {
            return this.$comparsion.val();
        },

        setEqMode(mode) {
            this.$comparsion.val(mode);
        },

        /**
         * Тип значения фильтра
         * @return string
         */
        getType() {
            return 'date';
        },

    });

});
