define(function(require) {
    'use strict';

    const promise = require('promise');

    const View = require('view');
    const hideable = require('hideable');

    const tpl = require('text!./tpl/filters.html');
    const filterTpl = require('text!./tpl/filter.html');
    const selectorTpl = require('text!./tpl/selector.html');
    require('css!./styles/filters.css');

    const filters = {};
    filters.bool = require('./filters/bool/bool-filter');
    filters.date = require('./filters/date/date-filter');
    filters.tags = require('./filters/tags/tags-filter');
    filters.text = require('./filters/text/text-filter');
    filters.period = require('./filters/period/period-filter');
    filters['date-between'] = require('./filters/date-between/date-between-filter');

    /**
     * Вернуть класс фильтра по его типу
     * @param  {string} type
     * @return {function}
     */
    function getFilter(type) {
        if(filters[type] === undefined) {
            throw new Error(`Filter with type "${type}" not found`);
        }
        return filters[type];
    }

    /**
     * Создать экземпляр фильтра из настроек data, подгрузить данные и обработать их
     * @param  {object} data настройки фильтра из availableFilters
     * @return {filter}
     */
    function makeFilter(data) {
        return new (getFilter(data.type))(_.omit(data, 'type'));
    }

    const ExtView = View.extend(hideable);

    return ExtView.extend({

        template: _.template(tpl),
        filterTemplate: _.template(filterTpl),
        selectorTemplate: _.template(selectorTpl),

        // Доступные для выбора фильтры
        availableFilters: [
            {type: 'tags', name: 'course', label: 'Курс'},
            {type: 'date', name: 'createdAt', label: 'Создан'},
            {type: 'bool', name: 'paid', label: 'Оплачен'},
            {type: 'bool', name: 'gift', label: 'Подарок'},
            {type: 'text', name: 'text', label: 'Text'},
        ],

        events: {
            'click [data-role="add"]': 'onAddClick',
            'click [data-role="remove"]': 'onRemoveClick',
            'change [data-role="type-selector"]': 'onFilterTypeChange',
            'click [data-role="apply"]': 'onApplyClick',
        },

        initialize(config) {
            this.$el.html(this.template);

            _.extend(this, _.pick(config, 'availableFilters'));

            this.findInAvailableFilters = _.findWhere.bind(this, this.availableFilters);

            this.filters = [];

            this.$filters = this.$('.filters-panel__items');

            this.selectorHtml = this.selectorTemplate(
                _.pick(this, 'availableFilters')
            );
        },

        /**
         * Добавить новый экземпляр фильтра name на панель
         * @param  {string}         name    [description]
         * @param  {object|null}    options {value, mode}
         * @return {filter}
         */
        appendFilter(name, options) {
            let available;
            if( !(available = this.findInAvailableFilters({name})) ) {
                throw new Error(`Filter with name "${name}" not available`);
            }

            const filter = makeFilter(available);
            // Переданы options - применяем значения в фильтр
            if(options) {
                const applyValues = function(filter, options) {
                    if(options.value !== undefined) {
                        filter.setInputValue(options.value);
                    }
                    if(options.mode !== undefined) {
                        filter.setEqMode(options.mode);
                    }
                }.bind(null, filter, options);

                if(filter.isRendered) {
                    applyValues();
                }
                else {
                    filter.once('rendered', applyValues);
                }
            }
            this.filters.push(filter);

            const $wrapper = $(this.filterTemplate());
            $wrapper.data('filter', filter);
            $(this.selectorHtml).val(filter.getName()).appendTo($wrapper.find('.filters__type-selector'));
            $wrapper.find('[data-role="filter"]').append(filter.$el);
            this.$filters.append($wrapper);

            return filter;
        },

        /**
         * Удаляет фильтр
         * @param  {[type]} filter [description]
         */
        removeFilter(filter) {
            this.filters = _.reject(this.filters, item => item === filter);
            filter.remove();
        },

        /**
         * Заменяет один фильтр другим
         * @param  {array}  data           [description]
         * @param  {el}     $filterWrapper [description]
         */
        replaceFilter(data, $filterWrapper) {
            const filter = makeFilter(data);
            this.filters.push(filter);
            this.removeFilter(
                $filterWrapper.data('filter')
            );
            $filterWrapper.data('filter', filter);
            $filterWrapper.find('[data-role="filter"]').append(filter.$el);
        },

        /**
         * [getValue description]
         * @return {array}
         * [
         *     {"name":"search","mode":"=","value":""},
         *     {"name":"course","mode":"=","value":["13","26"]},
         *     {"name":"createdAt","mode":"req","value":"2017-04-11"},
         *     {"name":"createdAt","mode":"leq","value":"2017-04-29"}
         * ]
         */
        getValue() {
            return this.filters
                .filter(filter => !filter.isEmpty())
                .map(filter => filter.getValue());
        },

        getFilters() {
            return _.clone(this.filters);
        },


        onAddClick(e) {
            let filter;
            if( !(filter = _.first(this.availableFilters)) ) {
                throw new Error(`Empty available filters list`);
            }
            this.appendFilter(filter.name);
        },

        onRemoveClick(e) {
            const $filter = $(e.target).parents('.filters__filter');
            this.removeFilter(
                $filter.data('filter')
            );
            $filter.remove();
        },

        onFilterTypeChange(e) {
            const $t = $(e.target);
            const name = $t.prop('value');

            this.replaceFilter(
                this.findInAvailableFilters({name}),
                $t.parents('.filters__filter')
            );
        },

        onApplyClick(e) {
            this.trigger('apply', this.getValue());
        },

    });

});
