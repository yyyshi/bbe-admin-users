(function(root, factory) {
    if(typeof define === 'function' && define.amd) {
        // AMD
        define([
            'jquery'
        ], factory);
    }
    else if(typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = factory(
            require('jquery')
        );
    }
    else {
        // Browser globals (root is window)
        root.returnExports = factory(
            root.jQuery
        );
    }
// Babel exports [this] as undefined
}((window || module || {}), function($, undefined) {
    'use strict';

    var pluginName = 'message';

    const defaults = {
        size: 'm',
        unclosable: true, // Нет кнопки закрытия
        appendTo: 'body',
        buttons: 'close', // 'close yes no ok cancel'
        type: 'default', // default | error
        callback: (role, action) => {},
    };

    const tpl = `
        <div>
            <input class="message-box__hide" type="checkbox" id="modal">
            <div class="message-box">
                <label class="message-box__close message-box__veil" for="modal"></label>
                <div class="message-box__close-button-wrapper">
                    <label class="message-box__close message-box__close-button" for="modal">&times;</label>
                </div>
                <div class="message-box__body">
                    <div class="message-box__content-wrapper">
                        <div class="message-box__icon"></div>
                        <div class="message-box__content"></div>
                    </div>
                    <div class="message-box__buttons">
                        <button class="button button_s button_default" data-role="close" data-action="false">
                            Закрыть
                        </button>
                        <button class="button button_s button_primary" data-role="yes" data-action="true">
                            Да
                        </button>
                        <button class="button button_s button_default" data-role="no" data-action="false">
                            Нет
                        </button>
                        <button class="button button_s button_primary" data-role="ok" data-action="true">
                            OK
                        </button>
                        <button class="button button_s button_default" data-role="cancel" data-action="false">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </div>
    `;

    function uuid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
    }

    /**
     * @events:
     *     modal:visibilityChange
     *     modal:show
     *     modal:hide
     */
    function Message(msg, options = {}, immediately = true) {
        let opts = $.extend({}, defaults, options);

        const id = uuid();
        const $el = $(tpl);
        $el.find('[id]').prop('id', getId());
        $el.find('[for]').prop('for', getId());
        if(opts.unclosable) {
            // Чтобы нельзя было закрыть
            $el.find('[for]').removeAttr('for');
        }
        $el.find('.message-box').addClass(`message-box_${opts.size}`);
        const $visibilityTrigger = $el.find('.message-box__hide');
        const $content = $el.find('.message-box__content');
        const $buttonsBox = $el.find('.message-box__buttons');
        const $buttons = $buttonsBox.find('.button');

        $el.find('.message-box__icon').addClass(`message-box__icon_${opts.type}`);

        // Скроем только ненужные кнопки
        $buttons.not(opts.buttons.split(' ').map(role => `[data-role=${role}]`).join(',')).hide();

        setContent(msg);

        if(immediately) {
            return show();
        }

        /*******************************/

        /**
         * Заменить html в content
         */
        function setContent(content) {
            $content.html(content);
            return this;
        }

        function isVisible() {
            return $visibilityTrigger.prop('checked');
        }

        function show() {
            return new Promise(function(resolve) {
                $el.appendTo(opts.appendTo);
                $visibilityTrigger.prop('checked', true);
                $buttonsBox.one('click', onButtonClick.bind(this, resolve));
            }.bind(this));
            return promise;
        }

        function hide() {
            $visibilityTrigger.prop('checked', false);
            $el.remove();
            return this;
        }

        function getId() {
            return `message-${id}`;
        }

        function onButtonClick(resolve, e) {
            const $t = $(e.target);
            opts.callback.call(this, $t.data('role'), $t.data('action'));
            const result = {
                role: $t.data('role'),
                action: !!$t.data('action')
            };
            resolve.call(this, result);
            hide();
        }

        return {
            setContent,
            isVisible,
            getId,
            show,
            hide,
        };
    }

    return Message;

}));
