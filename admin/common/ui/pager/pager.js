define(function(require) {
    'use strict';

    var Backbone = require('backbone');
    var _ = require('underscore');

    var tpl = require('text!./tpl/main.html');

    require('css!./styles/pager');

    /**
     * Events:
     *     navigate:page
     *     view:show
     *     view:hide
     */
    return Backbone.View.extend({

        visiblePages: 7,

        template: _.template(tpl),

        events: {
            'click .pager__link': 'onPageClick',
            'change .pager__per-page': 'onPerPageChange',
        },

        initialize(config) {
            this.$el.addClass('pager');

            this.visiblePages = config.visiblePages || this.visiblePages;

            this.listenTo(this.collection, 'reset', () => this.render({
                pagination: this.collection.getPagination(),
                currentPage: this.collection.getPagination().currentPage(),
                minPage: this.firstItem(),
                maxPage: this.lastItem(),
            }));

        },

        render({ pagination, minPage, maxPage, currentPage }) {
            if(!pagination) {
                this.hide();
                return this;
            }

            this.$el.html(this.template({
                minPage,
                maxPage,
                currentPage,
                totalPages: pagination.totalPages(),
            }));

            this.$('.pager__per-page').val(pagination.perPage());

            if(pagination.count()) {
                this.show();
            }
            else {
                this.hide();
            }

            return this;
        },

        visuallySelect(page) {
            this.$el.find('.pager__page_current').removeClass('pager__page_current')
                .end().find(`.pager__page[data-page="${page}"]`).addClass('pager__page_current');
        },

        selected() {
            return this.collection.getPagination().currentPage();
        },

        /**
         * Show view
         * @event view:show
         */
        show() {
            this.$el.show();
            this.trigger('show', this);
            return this;
        },

        /**
         * Hide view
         * @event view:hide
         */
        hide() {
            this.$el.hide();
            this.trigger('hide', this);
            return this;
        },

        // Первая видимая страница
        firstItem() {
            const pagination = this.collection.getPagination();
            let result = (pagination.currentPage() - Math.ceil(this.visiblePages / 2)) + 1;
            if(pagination.maxPage() - result < this.visiblePages) {
                result = pagination.maxPage() - this.visiblePages + 1;
            }
            return Math.max(1, result);
        },

        // Последняя видимая страница
        lastItem() {
            const pagination = this.collection.getPagination();
            let result = (pagination.currentPage() + Math.ceil(this.visiblePages / 2)) - 1;
            if(result < this.visiblePages) {
                result = this.visiblePages;
            }
            return Math.min(pagination.maxPage(), result);
        },

        getCollection() {
            return this.collection;
        },

        getTotal() {
            return this.getCollection().getTotal();
        },

        onPageClick(e) {
            e.preventDefault();
            const page = $(e.currentTarget).data('page');
            if(!page) {
                return;
            }
            this.visuallySelect(page);
            this.trigger('navigate:page', +page);
        },

        onPerPageChange(e) {
            const pageSize = $(e.currentTarget).val();
            this.trigger('navigate:page-size', +pageSize);
        },

    });

});
