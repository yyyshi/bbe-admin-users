define(function(require, exports, module) {
    'use strict';

    const rivets = require('rivets');
    const util = require('common/utils/util');

    /**
     *  money: {
     *      amount: 17000,
     *      currency: {
     *          code: "RUB"
     *      }
     *  }
     */
    rivets.formatters.money = function(money) {
        if(money === undefined || money.amount === undefined || money === null || money.amount === null) {
            return;
        }

        return `<span class="money">
                    <span class="money__amount">
                        ${util.string.numberFormat(money.amount, null, null, '&nbsp;')}
                    </span>
                    <span class="currency currency_${money.currency.code.toLowerCase()}"></span>
                </span>`;
    };

});
