define(function(require, exports, module) {
    'use strict';

    var rivets = require('rivets');

    // select/option/@selected
    rivets.binders['selected'] = {

        bind: function(el) {
        },

        unbind: function(el) {
        },

        routine: function(el, value) {
            if(value) {
                el.setAttribute('selected', '');
            }
            else {
                el.removeAttribute('selected');
            }
        },

        getValue: function(el) {
            return el.hasAttribute('selected');
        }

    };

});
