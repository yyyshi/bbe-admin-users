﻿define(function(require, exports, module) {
    'use strict';

    const rivets = require('rivets');
    const util = require('common/utils/util');
    const logical = require('common/utils/logical');

    rivets.formatters = Object.assign({}, rivets.formatters, logical);

    /**
     * Usage:
     *     <span rv-text="item:days | humanSense 'день' 'дня' 'дней'"></span>
     * @param  {number} value
     * @param  {string} first   1 товарная позиция
     * @param  {string} second  3 товарные позиции
     * @param  {string} third   5 товарных позиций
     * @param  {string} nothing 0
     * @return {string}
     */
    rivets.formatters.humanSense = function(value, first, second, third, nothing) {
        if(!value) return nothing;

        var residue = (value % 10);
        var residue2 = (value % 100);

        if(residue == 1) return first;
        else if((residue2 >= 11 && residue2 <= 14) || residue === 0 || (residue >= 5 && residue <= 9)) return third;
        else if(residue >= 2 && residue <= 4) return second;
    };

    rivets.formatters.numberFormat = function(number, decimals, decPoint, thousandsSep) {
        if(number === undefined || number === null || isNaN(number)) {
            return;
        }
        return util.string.numberFormat.apply(null, arguments);
    };

    // Оборачивает name в leftWrapper и rightWrappers (например, для level1[name])
    // Понимает значения внутри скоупа rivets
    // rv-name="question.id | wrapString 'question[' '][answers][' answer.id '][type]'"
    rivets.formatters.wrapString = function(name = '', leftWrapper, ...rightWrappers) {
        return leftWrapper + name + rightWrappers.join('');
    };

    rivets.formatters.entitiesDecode = function(str) {
        return util.string.entitiesDecode(str);
    };

    rivets.formatters.toLowerCase = function(str) {
        if(!str) {
            return str;
        }
        return str.toLowerCase();
    };

    rivets.formatters.inc = function(val, incValue = 1) {
        return +val + incValue;
    };

    rivets.formatters.dec = function(val, incValue = 1) {
        return +val - incValue;
    };

    rivets.formatters.round = function(val) {
        return Math.round(val);
    };

    rivets.formatters.multiply = function(val, factor) {
        return val * factor;
    };

    rivets.formatters.divide = function(val, divisor) {
        return val / divisor;
    };

    rivets.formatters.length = function(val) {
        if(val === undefined) {
            return;
        }
        return val.length;
    };

});
