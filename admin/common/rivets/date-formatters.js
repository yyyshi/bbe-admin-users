define(function(require, exports, module) {
    'use strict';

    var rivets = require('rivets');
    var fecha = require('fecha');

    // https://github.com/taylorhakes/fecha
    fecha.i18n = {
        monthNames: ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
    };

    // @TODO помойка, надо б переписать
    function matchFormat(date) {
        if(!matchFormat[date]) {
            matchFormat[date] = date.match(/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}/)[0];
        }
        const matchDate = matchFormat[date];
        const matchTime = date.match(/([0-9]{2}\:[0-9]{2})/);
        return {
            match: matchDate ? !!matchDate[0] : false,
            hasTime: matchTime ? !!matchTime[0] : false,
        };
    }

    rivets.formatters.dateInterval = function(from, till) {
        if(!from || !till) {
            return;
        }
        var now = new Date();
        var fromParsed = fecha.parse(from, 'YYYY-MM-DD');
        var tillParsed = fecha.parse(till, 'YYYY-MM-DD');
        var displayFromFormat = 'D';
        var displayTillFormat = 'D MMMM';

        if(fecha.format(fromParsed, 'YYYY') !== fecha.format(tillParsed, 'YYYY') ||
           fecha.format(tillParsed, 'YYYY') !== fecha.format(now, 'YYYY')) {
            displayTillFormat += ' YYYY';
        }
        if(fecha.format(fromParsed, 'MM') !== fecha.format(tillParsed, 'MM')) {
            displayFromFormat += ' MMMM';
        }
        if(fecha.format(fromParsed, 'YYYY') !== fecha.format(tillParsed, 'YYYY') &&
           fecha.format(fromParsed, 'YYYY') !== fecha.format(now, 'YYYY')) {
            displayFromFormat += ' YYYY';
        }

        return fecha.format(fromParsed, displayFromFormat) + '&mdash;' + fecha.format(tillParsed, displayTillFormat);
    };

    /**
     * Date is past
     * @param  {[type]}  date [description]
     * @return {Boolean}      [description]
     */
    rivets.formatters.isPast = function(date) {
        if(!date) {
            return;
        }
        var now = new Date();
        var nowParsed = fecha.parse(fecha.format(now, 'YYYY-MM-DD'), 'YYYY-MM-DD');
        var dateParsed = fecha.parse(date, 'YYYY-MM-DD');

        return nowParsed > dateParsed;
    };

    rivets.formatters.isFutureOrNow = function(date) {
        if(!date) {
            return;
        }
        var now = new Date();
        var nowParsed = fecha.parse(fecha.format(now, 'YYYY-MM-DD'), 'YYYY-MM-DD');
        var dateParsed = fecha.parse(date, 'YYYY-MM-DD');

        return nowParsed <= dateParsed;
    };

    rivets.formatters.dateFormat = function(date, format, forceYear) {
        if(!date || !matchFormat(date).match) {
            return;
        }
        const hasTime = matchFormat(date).hasTime;
        format = format || 'D MMMM YYYY';
        forceYear = forceYear || false;
        var now = new Date();
        var dateParsed = fecha.parse(date, `YYYY-MM-DD${(hasTime ? ' HH:mm' : '')}`);
        if(!dateParsed) {
            return;
        }
        if(!forceYear && fecha.format(dateParsed, 'YYYY') === fecha.format(now, 'YYYY')) {
            format = format.replace('YYYY', '').trim();
        }

        return fecha.format(dateParsed, format);
    };

    rivets.formatters.humanDate = function(date, showTime = true) {
        if(!date || !matchFormat(date).match) {
            return;
        }

        const hasTime = matchFormat(date).hasTime;

        var now = new Date();
        var dateParsed = fecha.parse(date, `YYYY-MM-DD${(hasTime ? ' HH:mm' : '')}`);
        var result = rivets.formatters.dateFormat(date);
        var diff = (now.getTime() - dateParsed.getTime()) / 1000;

        if(fecha.format(dateParsed, 'DD-MM-YYYY') == fecha.format(now, 'DD-MM-YYYY')) {
            result = 'Сегодня';
        }
        else if(fecha.format(dateParsed, 'MM-YYYY') == fecha.format(now, 'MM-YYYY') &&
                +fecha.format(dateParsed, 'D') + 1 == +fecha.format(now, 'D')) {
            result = 'Вчера';
        }

        return `${result}${(hasTime && showTime ? ', ' + rivets.formatters.dateFormat(date, 'HH:mm') : '')}`;
    };

});
