define(function(require, exports, module) {
    'use strict';

    const rivets = require('rivets');
    const _ = require('underscore');

    // Сортирует items по ключу sortkey
    // rv-each-email="enrollment:course.emails | sortBy"
    rivets.formatters.sortBy = function(items, sortkey = 'sortkey', dir = 'asc') {
        if(!items) {
            return items;
        }

        if(dir.toLowerCase() === 'asc') {
            return _.sortBy(items, sortkey);
        }
        return _.sortBy(items, sortkey).reverse();
    };

    // Группировка по определенному полю модели
    // Поле showGroupName = false | true, надо ли отображать
    rivets.formatters.groupBy = function(items, by = 'sortkey') {
        if(!items) {
            return items;
        }

        return _.sortBy(items, by).map(function(item, idx, all) {
            if(idx && all[idx - 1].get(by) == item.get(by)) {
                item.set('showGroupName', false);
            }
            else {
                item.set('showGroupName', true);
            }
            return item;
        });
    };

});
