﻿define(function(require, exports, module) {
    'use strict';

    const rivets = require('rivets');

    const events = 'add remove update reset sort change';

    rivets.adapters[':'] = {

        observe(obj, keypath, callback) {
            if('models' in obj) {
                obj.on(events, callback);
            }
            else {
                obj.on('change:' + keypath, callback);
            }
        },
        unobserve(obj, keypath, callback) {
            obj.off('change:' + keypath, callback);
            obj.off(events, callback);
        },

        get(obj, keypath) {
            // collection or model
            var value =  ('models' in obj) ? obj.models : obj.get(keypath);
            return value;
        },

        set(obj, keypath, value) {
            obj.set(keypath, value);
        },

    };

});
