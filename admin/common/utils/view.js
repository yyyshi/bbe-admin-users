define(function(require) {
    'use strict';

    const promise = require('promise');

    function toggleClass(className, $buttons, state) {
        $buttons[state ? 'removeClass' : 'addClass'](className);
    }

    function loadImage(src) {
        return new promise(function(resolve) {
            const img = new Image();
            img.onload = function() {
                resolve(this);
            };
            img.src = src;
        });
    }


    function query(collection) {
        return collection.query()
            .catch(err => {
                throw err;
            });
    }


    function collectionsCompareAndSelect(checkableCollection, comparedCollection, filtratorFn) {
        return checkableCollection.map(
            itemsSelector.bind(comparedCollection, filtratorFn)
        );
    }

    function itemsSelector(filtratorFn, item) {
        return item.set(
            'selected',
            !!this.filter(filtratorFn.bind(item)).length
        );
    }

    // Фильтратор по item.id
    function idFiltrator(item) {
        return item.id === this.id;
    }

    // Определяет ширину скроллбара
    function getScrollBarWidth() {
        // Create the measurement node
        var scrollDiv = document.createElement("div");
        scrollDiv.style.width = '100px';
        scrollDiv.style.height = '100px';
        scrollDiv.style.overflow = 'scroll';
        scrollDiv.style.position = 'absolute';
        scrollDiv.style.top = '-9999px';
        document.body.appendChild(scrollDiv);

        // Get the scrollbar width
        var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;

        // Delete the DIV
        document.body.removeChild(scrollDiv);

        return scrollbarWidth;
    }

    return {

        toggleClass,
        loadImage,

        query,

        collectionsCompareAndSelect,
        itemsSelector,
        idFiltrator,

        getScrollBarWidth,

    };

});
