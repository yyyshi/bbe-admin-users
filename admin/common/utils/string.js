define(function(require, exports, module) {
    'use strict';

    function memoize(fn, key, value) {
        fn.cache = fn.cache || {};
        if(value === undefined) {
            return fn.cache[key];
        }
        fn.cache[key] = value;
        return value;
    }

    /**
     * Uppercase First Character Of Each Word
     * @param  {[type]} str [description]
     * @return {[type]}     [description]
     */
    function ucwords(str) {
        const cache = memoize.bind(null, ucwords, str);

        if(cache()) {
            return cache();
        }

        return str.split(' ')
            .map(word => `${word[0].toUpperCase()}${word.substr(1).toLowerCase()}`)
            .join('');
    }

    /**
     * lowerCase first character
     * @param  {[type]} str [description]
     * @return {[type]}     [description]
     */
    function lcfirst(str) {
        const cache = memoize.bind(null, lcfirst, str);

        if(cache()) {
            return cache();
        }

        return `${word[0].toLowerCase()}${word.substr(1)}`;
    }

    /**
     * StudlyCase
     * @param  string  str
     * @return string
     */
    function studly(str) {
        const cache = memoize.bind(null, studly, str);

        if(cache()) {
            return cache();
        }

        str = ucwords(str.replace(/[\-\_]/g, ' '))
        return str.replace(' ', '');
    }

    /**
     * camelCase
     * @param  {[type]} str [description]
     * @return {[type]}     [description]
     */
    function camel(str) {
        const cache = memoize.bind(null, camel, str);

        if(cache()) {
            return cache();
        }

        return lcfirst(studly(str));
    }

    return {

        lcfirst,
        ucwords,
        studly,
        camel,

    };

});
