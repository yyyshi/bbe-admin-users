define(function(require) {
    'use strict';

    const eq = (a, b, strict = true) => strict ? a === b : a == b;

    const neq = (a, b, strict = true) => strict ? a !== b : a != b;

    const gt = (a, b) => a > b;

    const gte = (a, b) => a >= b;

    const lt = (a, b) => a < b;

    const lte = (a, b) => a <= b;

    const and = (a, b) => a && b;

    const or = (a, b) => a || b;

    const not = a => !a;

    return {
        eq,
        neq,
        gt,
        gte,
        lt,
        lte,
        and,
        or,
        not,
    };

});
