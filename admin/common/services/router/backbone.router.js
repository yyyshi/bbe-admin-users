﻿define(function (require) {
    'use strict';

    var _ = require('underscore');
    var Backbone = require('backbone');

    return Backbone.Router.extend({

        initialize() {
            this.history = [];
            this.listenTo(this, 'route', (name, args) => {
                this.history.push({
                    name: name,
                    args: args,
                    fragment: Backbone.history.fragment
                });
            });
        },

        parseQueryString(qs) {
            if(!qs) {
                return {};
            }

            return _.object(
                decodeURI(qs).split('&')
                    .map(this.paramsSplit)
                    .map(([i0, i1]) => [i0, i1 ? JSON.parse(i1) : i1 ])
            );
        },

        /**
         * Разбивает строку параметр=значение на две части
         * JSON-safe
         * @param  {string} str [description]
         * @return {array}  [key, val]
         */
        paramsSplit(str) {
            const eqi = str.indexOf('=');
            return [
                str.slice(0, eqi),
                str.slice(eqi + 1),
            ]
        },

        // Убирает откр. и закр. слэши
        trimSlashes(str) {
            // @TODO refactor
            return str.split('/').reduce((carry, item, idx, all) => {
                if((idx !== 0 || idx !== all.length - 1) && item !== '') {
                    carry.push(item);
                }
                return carry;
            }, []).join('/');
        },

        // Добавляет закрывающий слэш к str
        forceTrailingSlash(str) {
            return this.trimSlashes(str) + '/';
        },

        popLastHistory() {
            if(this.history.length <= 1) {
                this.history = [];
                return null;
            }

            // Т.к. последняя запись - это текущая страница, вернем предпоследнюю
            var result = this.history.slice(-2, -1).shift();
            this.history.splice(-2);
            return result;
        },

        /**
         * Разбирает урл
         * @param  {String} urlString typografika/?page=4&search=design
         * @return {Object}           { url:String, queryString:String, params:Object }
         */
        splitUrl(urlString) {
            var result = {};
            var parts = urlString.split('?');
            result.url = parts[0];
            result.queryString = parts[1] ? decodeURI(parts[1]) : '';
            result.params = this.parseQueryString(result.queryString);
            return result;
        },

        /**
         * Собирает урл
         * @param  {String} url               'typografika/'
         * @param  {Object|null} paramsObject { page: 4, search: 'design' }
         * @return {String}                   'typografika/?page=4&search=design'
         */
        buildUrl(url, paramsObject) {
            paramsObject = paramsObject || {};
            var partsArray = [url];
            var queryString = this.concatParams(paramsObject);
            if(queryString) {
                partsArray.push(queryString);
            }
            return encodeURI(partsArray.join('?'));
        },

        /**
         * Собирает параметры
         * @param  {Object} paramsObject { page: 4, search: 'design' }
         * @return {String}              'page=4&search=design'
         */
        concatParams(paramsObject) {
            paramsObject = paramsObject || {};

            return _.pairs(paramsObject)
                // Конвертим значения в JSON, если не пустое
                .map(([i0, i1]) => [i0, i1 = _.isNull(i1) || _.isUndefined(i1) ? '' : JSON.stringify(i1)])
                .map(pair => pair.join('=')).join('&');
        },

    });

});
