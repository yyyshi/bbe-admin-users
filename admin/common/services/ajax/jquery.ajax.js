define(function(require) {
    'use strict';

    const $ = require('jquery');
    const ajax = $.ajax;
    const promise = require('promise');

    const defaults = {
    };

    function ajaxQuery(options = {}) {
        const opts = Object.assign({}, defaults, options);

        return new promise(function(resolve, reject) {
            ajax(options)
                .done(function(response, message, xhr) {
                    if(message === 'success') {
                        resolve(response);
                    }
                    reject(response);
                })
                .fail(function(xhr, error, message) {
                    reject(xhr.responseJSON || message);
                });
        });
    }

    function get(options = {}) {
        const opts = Object.assign({}, options, { method: 'GET' });

        return ajaxQuery(opts);
    }

    function post(options = {}) {
        const opts = Object.assign({}, options, { method: 'POST' });

        return ajaxQuery(opts);
    }

    function put(options = {}) {
        const opts = Object.assign({}, options, { method: 'PUT' });

        return ajaxQuery(opts);
    }

    function del(options = {}) {
        const opts = Object.assign({}, options, { method: 'DELETE' });

        return ajaxQuery(opts);
    }

    return {
        ajax: ajaxQuery,
        get,
        post,
        put,
        delete: del,
    };

});
