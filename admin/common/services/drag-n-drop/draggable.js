define(function(require) {
    'use strict';

    require('css!./draggable.css');

    const $document = $(document);

    function onMouseDown(e) {
        e.preventDefault();
        this.$dragEl = $(e.currentTarget);

        this.mouse.offsetX = (e.offsetX !== undefined ? e.offsetX : e.pageX);
        this.mouse.offsetY = (e.offsetY !== undefined ? e.offsetY : e.pageY);

        this.$placeholder = this.createPlaceholder()
            .insertAfter(this.$dragEl)
            .addClass(this.$dragEl.attr('class'))
            .css({
                width: this.$dragEl.outerWidth(),
                height: this.$dragEl.outerHeight(),
            });

        this.$dragEl.addClass('draggable__dragged');

        $document.on('mousemove', onMouseMove.bind(this));
        $document.on('mouseup', onMouseUp.bind(this));

        this.$el.trigger('draggable:drag', this.$dragEl);
    }

    function onMouseMove(e) {
        let xy = {
            x: e.pageX - this.mouse.offsetX,
            y: e.pageY - this.mouse.offsetY,
        }
        this.$dragEl.css({
            left: xy.x + 'px',
            top: xy.y + 'px',
        });

        this.pointEl = document.elementFromPoint(xy.x - 1, xy.y - 1);

        let $newLocation = $(this.pointEl).closest('.draggable');
        if($newLocation.is(':first-child')) {
            this.$placeholder.insertBefore($newLocation);
        }
        else {
            this.$placeholder.insertAfter($newLocation);
        }

    }

    function onMouseUp(e) {
        this.$dragEl.removeClass('draggable__dragged')
            .css({ left: '', top: '' });
        this.$dragEl.insertAfter(this.$placeholder);
        this.$placeholder.remove();
        $document.off('mousemove');
        $document.off('mouseup');

        this.$el.trigger('draggable:drop', [this.$dragEl]);
    }

    function debugInit() {
        const $debug = $('<div>').css({
            position: 'fixed',
            top: '10px',
            left: '10px',
            background: '#fff',
            padding: '10px',
            zIndex: 9999,
        }).appendTo('body');
        return function(data = '', clean = false) {
            let html = '';
            if(!clean) {
                html = $debug.html();
            }
            $debug.html(`${html}${(html ? '<br>' : '')}${data}`);
        }
    }

    return {

        init(el) {
            this.$el = $(el);

            this.mouse = {};
            this.$dragEl;

            this.$el.on('mousedown', '.draggable', onMouseDown.bind(this));
        },

        createPlaceholder() {
            return $('<div class="draggable__placeholder">');
        },

    };

});
