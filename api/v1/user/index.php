<?php

header('Content-type: application/json');

preg_match('/[a-z0-9]{8}\-[a-z0-9]{4}\-[a-z0-9]{4}\-[a-z0-9]{4}\-[a-z0-9]{12}/', $_SERVER['REQUEST_URI'], $matches);

$name = $matches[0] . '.json';
if( !file_exists($name) ) {
    header('HTTP/1.1 404 Not Found');
    die();
}

print file_get_contents($matches[0] . '.json');
