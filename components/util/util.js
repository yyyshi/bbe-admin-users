;(function(global, undefined) {
    'use strict';

    var util = namespace('Bb.util');
    util.string = namespace('Bb.util.string');
    util.math = namespace('Bb.util.math');
    util.dom = namespace('Bb.util.dom');
    util.location = namespace('Bb.util.location');


    // Для Safari
    global.URL = global.URL || global.webkitURL;

    /**
     * Create namespaces
     * Usage:
     *     Bb.util.namespace('parent.child.subchild');
     *     Bb.util.namespace('parent.child.subchild', 'ns1.ns2.ns3');
     *
     * @return {[type]} [description]
     */
    function namespace() {
        var d;
        var args = arguments;
        var o;

        for(var i = 0, argsLen = args.length; i < argsLen; i = i + 1) {
            d = args[i].split('.');
            o = global;
            for(var j = 0, dLen = d.length; j < dLen; j = j + 1) {
                o[ d[j] ] = o[ d[j] ] || {};
                o = o[ d[j] ];
            }
        }

        return o;
    };

    /**
     * Detect browser & os
     *
     * @return {[type]} [description]
     */
    function getBrowserData(ua) {
        var ua, engine, vendor, os, mobile;

        engine = ua.match(/(webkit|gecko|presto|trident)/ig);
        vendor = ua.match(/(firefox|safari|chrome|opera|msie)/ig);
        os = ua.match(/(os\sx|windows|linux)/ig);
        mobile = ua.match(/(mobile)/ig);

        try {
            engine = (engine && engine[0].toLowerCase()) || 'unknown';
            vendor = (vendor && vendor[0].toLowerCase()) || 'unknown';
            os = (os && os[0].replace(' ', '').toLowerCase()) || 'unknown';
            mobile = (mobile ? true : false);
        }
        catch(e) {
            global.console && console.log(e, e.message);
        }

        return {
            engine: engine, // webkit | gecko | presto | trident
            vendor: vendor, // firefox | safari | chrome | opera | msie
            os: os,         // windows | linux | osx
            mobile: mobile  // true | false
        }
    }

    var browserData = getBrowserData(navigator.userAgent.toLowerCase());

    /**
     * Alias of Bb.util.namespace()
     * @return {[type]} [description]
     */
    Bb.ns = namespace;
    util.namespace = namespace;

    /**
     * Текущий UA
     * @type {[type]}
     */
    util.ua = browserData;

    /**
     * Strip tags from string
     * @param  {string} str [description]
     * @return {[type]}     [description]
     */
    util.string.stripTags = function(str) {
        return !str ? str : String(str).replace(/<\/?[^>]+>/gi, '');
    };

    /**
     * (\r)\n to <br>
     * @param  {[type]} str [description]
     * @return {[type]}     [description]
     */
    util.string.nl2br = function(str) {
        return str ? str.replace(/\r?\n/g, '<br>') : str;
    };

    /**
     * <br>,<br/>,<br /> to \r\n
     * @param  {[type]} str [description]
     * @return {[type]}     [description]
     */
    util.string.br2nl = function(str) {
        return str ? str.replace(/<br(\s*\/?)>/g, '\r\n') : str;
    };

    /**
     * форматирует вывод числа, аналог number_format() в PHP
     * @param  {[type]} number        [description]
     * @param  {[type]} decimals      [description]
     * @param  {[type]} decPoint     [description]
     * @param  {[type]} thousandsSep [description]
     * @return {[type]}               [description]
     */
    util.string.numberFormat = function(number, decimals, decPoint, thousandsSep) {
        var i, z, temp, sign, integer, fractional,
            exponent = '',
            numberstr = number.toString(),
            eindex = numberstr.indexOf('e');
        var result = '';

        if(number !== undefined) {
            thousandsSep || (thousandsSep = ' ');

            if(eindex > -1) {
                exponent = numberstr.substring(eindex);
                number = parseFloat(numberstr.substring(0, eindex));
            }

            decimals || (decimals = 0);
            if(decimals !== null) {
                temp = Math.pow(10, decimals);
                number = Math.round(number * temp) / temp;
            }
            sign = (number < 0 ? '−' : '');
            integer = (number > 0 ? Math.floor(number) : Math.abs(Math.ceil(number))).toString();

            fractional = number.toString().substring(integer.length + sign.length);
            decPoint = (decPoint !== null ? decPoint : '.');
            fractional = (decimals !== null && decimals > 0 || fractional.length > 1 ? decPoint + fractional.substring(1) : '');
            if(decimals !== null && decimals > 0) {
                for(i = fractional.length - 1, z = decimals; i < z; ++i) {
                    fractional += '0';
                }
            }

            thousandsSep = ((thousandsSep != decPoint || fractional.length === 0) ? thousandsSep : null);
            if(thousandsSep !== null && thousandsSep !== '') {
                for(i = integer.length - 3; i > 0; i -= 3) {
                    integer = integer.substring(0, i) + thousandsSep + integer.substring(i);
                }
            }

            result = sign + integer + fractional + exponent;
        }

        return result;
    };

    // Обрезает текст, чтобы влезло только целое предложение (до точки)
    // Usage: shrinkToMaxSentence(str, 70, true);
    util.string.shrinkToMaxSentence = function(str, len, addDotToEnd = false) {
        const addSplitter = str => (str.length ? '. ' : '');

        let sentences = str.split('.').map(s => s.trim()).filter(s => s.length);

        let result = sentences.reduce((carry, item) => {
            if((carry + item + addSplitter(carry)).length <= len - (addDotToEnd ? 1 : 0)) {
                carry += addSplitter(carry) + item;
            }
            return carry;
        }, '');

        return result + (addDotToEnd ? '.' : '');
    };

    // Конвертит snake_case или snake-case в CamelCase
    util.string.camelCase = function(string) {
        return string.replace(/(?:^|[\-\_])\w/g, match => match.replace(/[\-\_]/, '').toUpperCase());
    };

    /**
     * Extends objects
     * @param  {Object} Любое кол-во расширяемых объектов: obj, obj2, ..., objN
     * @return {Object} [description]
     */
    util.extend = function() {
        var key;
        var args = arguments;

        for(var i = 1, len = args.length; i < len; i++) {
            for(key in args[i]) {
                if(args[i].hasOwnProperty(key)) {
                    args[0][key] = args[i][key];
                }
            }
        }
        return args[0];
    };

    /**
     * [parseUrl description]
     * @param  {[type]} url [description]
     * @return window.location (https://developer.mozilla.org/en-US/docs/Web/API/window.location)
     */
    util.string.parseUrl = function(url) {
        var a = document.createElement('a');
        a.href = url;
        return a;
    };

    /**
     * Округление до n знаков после запятой
     * @param  {[type]} val Значение
     * @param  {[type]} n   Сколько знаков после запятой
     * @return {[type]}     [description]
     */
    util.math.decimalRound = function(val, n) {
        if(n > 0) {
            n = Math.pow(10, n);
            val = Math.round(val * n) / n;
        }

        return val;
    };

    /**
     * Возвращает соответствующее значение для числа
     * @param  {[type]} v       число
     * @param  {[type]} first   1 товарная позиция
     * @param  {[type]} second  5 товарных позиций
     * @param  {[type]} third   3 товарные позиции
     * @param  {[type]} nothing нет товарных позиций
     * @return {string}
     */
    util.string.humanSense = function(v, first, second, third, nothing) {
        if(!v) return nothing;

        var residue = (v % 10);
        var residue2 = (v % 100);

        if((residue2 >= 11 && residue2 <= 14) || residue === 0 || (residue >= 5 && residue <= 9)) return second;
        else if(residue == 1) return first;
        else if(residue >= 2 && residue <= 4) return third;
    };

    /**
     * Декодирует HTML Entity
     * Использует jQuery
     * @param  {string} str
     * @return {string}
     */
    util.string.entitiesDecode = function(str) {
        if(!$) {
            throw new Error('entitiesDecode() used jQuery');
        }
        return $('<textarea />').html(str).text();
    };

    /**
     * Возвращает объект с data-атрибутами элемента
     * data-atr1="1" data-atr2="2" => {atr1: "1", atr2: "2"}
     * @param  {Node} el [description]
     * @return {Object}
     */
    util.dom.getDataset = function(el) {
        var prefix = 'data-';
        var extractFrom = prefix.length;
        return Array.prototype.slice.call(el.attributes).reduce(function(carry, attr) {
            var name = attr.name;
            if(name.indexOf(prefix) === 0) carry[name.slice(extractFrom)] = attr.value;
            return carry;
        }, {});
    };

    util.location.redirect = function(to) {
        window.location = to;
    };

    /**
     * Prevent default then call fn
     * Usage:
     *     $('a').on('click', preventDefault(show, 'arg1', 'arg2'));
     * @param  {Function}  fn   [description]
     * @param  {mixed} args Arguments to fn(e, ...args)
     * @return {Function}
     */
    util.preventDefault = function(fn, ...args) {
        return function(e) {
            e.preventDefault();
            fn.apply(null, [e].concat(args));
        };
    };

    /**
     * Usage:
     *     delayedExec(function(value) {console.log(value);}, 500, val);
     * @param  {Function} fn    [description]
     * @param  {[type]}   delay [description]
     * @return {[type]}         [description]
     */
    util.delayedExec = function(fn, delay) {
        var args = Array.prototype.slice.call(arguments).slice(2);
        clear(fn);
        delayedExec[fn] = setTimeout(function() {
            fn.apply(null, args);
            clear(fn);
        }, delay, args);

        function clear(fn) {
            if(delayedExec[fn]) clearTimeout(delayedExec[fn]);
        }
    };

    /**
     * Устраняет повторные вызовы функции fn,
     * если между ними прошло менее delay ms
     * Usage:
     * $(window).resize(debounce(
     *     () => canvasBox($canvas.offset()),
     * 50));
     */
    util.debounce = function(fn, delay = 100) {
        let timeout;
        return (...args) => {
            clearTimeout(timeout);
            timeout = setTimeout(() => {
                fn.apply(null, args);
            }, delay);
        };
    };

    /**
     * Вызывает функцию fn только раз в threshold ms
     * @param  {Function} fn        [description]
     * @param  {[type]}   threshold [description]
     * @return {[type]}             [description]
     */
    util.throttle = function(fn, threshold = 100) {
        let prevTime;
        let timeout;
        return function() {
            const args = arguments;
            const now = +new Date();
            if(prevTime && now < prevTime + threshold ) {
                clearTimeout(timeout);
                timeout = setTimeout(function() {
                    prevTime = now;
                    fn.apply(null, args);
                }, threshold);
                return;
            }
            prevTime = now;
            fn.apply(null, args);
        };
    };

})(window);
