/**
 * Use jQuery Validation Plugin v1.13.1 (http://jqueryvalidation.org/)
 *
 */
;(function(window, $, undefined) {
    'use strict';

    var form = Bb.ns('Bb.form');

    const defaults = {
        errorElement: 'span',
        errorClass: 'input-validation-error',
        messageElement: 'span',
        messageClass: 'input-validation-message',
        onfocusout: false,
        onkeyup: function(el, e) {
            const code = e.which;
            if([9, 13, 16, 17, 18, 91, 93, 37, 38, 39, 40].indexOf(code) !== -1) return;
            // Очищаем статусы ошибки
            // this.resetForm();
            // Очищаем статусы ошибки только измененного элемента
            this.resetElement(el);
            this.hideErrorFor(el);
        },
    };

    // Валидация полей с одинаковыми @name, например, несколькими name="email[]"
    $.validator.prototype.checkForm = function() {
        this.prepareForm();
        for(var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
            if(this.findByName(elements[i].name).length != undefined && this.findByName(elements[i].name).length > 1) {
                for(var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                    this.check(this.findByName(elements[i].name )[cnt]);
                }
            }
            else {
                this.check(elements[i]);
            }
        }
        return this.valid();
    };

    $.validator.prototype.resetElement = function(element) {
        this.resetElements([element]);
    }

    $.validator.prototype.hideErrorFor = function(element) {
        $(element).next(this.settings.errorElement + '.' + this.settings.errorClass).remove();
    };

    form.validator = function(form, options) {
        const $form = $(form);
        const settings = $.extend({}, defaults, options);
        const validator = $form.validate(settings);
        let $error;
        let $message;

        function showError(error) {
            if(!$error) {
                $error = $(`<${settings.errorElement} class="${settings.errorClass}">`).appendTo($form);
            }
            $error.show().html(error);
        }

        function showMessage(message) {
            if(!$message) {
                $message = $(`<${settings.messageElement} class="${settings.messageClass}">`).appendTo($form);
            }
            $message.show().html(message);
        }

        function hideErrors() {
            if($message) {
                $message.remove();
                $message = null;
            }
        }

        return {
            showErrors: errors => validator.showErrors(errors),
            showError,
            showMessage,
            hideErrors,
        };
    };

})(window, jQuery);
